import data.Agreement
import data.Signatory
import utils.agreementMapper
import utils.agreementQuery
import utils.signatoryMapper
import java.sql.DriverManager
import utils.excelFilepath
import utils.mainWorkbook
import utils.processExcelContent
import utils.writeCSV
import utils.writeExcel
import kotlin.reflect.full.memberProperties

fun main(args: Array<String>) {
    // First argument is the db user (e.g. "lexadmin")
    // Second argument is the db password (e.g. "BestProductEver!23")
    val jdbcUrl = "jdbc:postgresql://localhost:5432/corporate"
    val connection = DriverManager.getConnection(jdbcUrl, args[0], args[1])

    if (!connection.isValid(0)) {
        return
    }

    connection.use {
        val agreementResult = connection.prepareStatement(agreementQuery("agreement")).executeQuery()
        val signatoryResult = connection.prepareStatement(agreementQuery("signatory")).executeQuery()

        val agreementData = agreementResult.use {
            agreementMapper(agreementResult)
        }
        val signatoryData = signatoryResult.use {
            signatoryMapper(signatoryResult)
        }
        // Process Excel Output
        processExcelContent("agreement", agreementData, Agreement::class.memberProperties)
        processExcelContent("signatory", signatoryData, Signatory::class.memberProperties)
        writeExcel(excelFilepath, mainWorkbook)

        // Process CSV Output
        writeCSV("signatory", signatoryData, Signatory::class.memberProperties)
        writeCSV("agreement", agreementData, Agreement::class.memberProperties)
    }
}
