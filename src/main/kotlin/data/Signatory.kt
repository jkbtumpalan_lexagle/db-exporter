package data

import java.time.OffsetDateTime

data class Signatory(
    val user_id: String,
    val version: Int,
    val added_by: String,
    val added_on: OffsetDateTime,
    val updated_by: String,
    val updated_on: OffsetDateTime,
    val deleted_by: String? = null,
    val deleted_on: OffsetDateTime? = null,
    val is_deleted: Boolean,
    val status: String,
    val order: Int,
    val id: String,
    val type: String,
    val signing_request_id: String,
    val info: String? = null
)
