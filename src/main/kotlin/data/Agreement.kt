package data

import java.time.OffsetDateTime

data class Agreement(
    val id: String,
    val file_version: Int,
    val file_name: String,
    val download_url: String,
    val preview_url: String,
    val version: Int,
    val added_by: String,
    val added_on: OffsetDateTime,
    val updated_by: String,
    val updated_on: OffsetDateTime,
    val deleted_by: String? = null,
    val deleted_on: OffsetDateTime? = null,
    val is_deleted: Boolean,
    val status: String,
    val type: String? = null,
    val workspace_id: String,
    val room_id: String,
    val room_name: String,
    val activity_id: String,
    val redline: String? = null, // jsonb
    val image_urls: String? = null, // jsonb
    val signing_metadata: String? = null, // jsonb
    val source_room_id: String? = null,
    val termination_reason: String? = null,
    val source_id: String? = null,
    val source_file_version: Int? = null,
    val file_source: String,
    val is_circulated: Boolean,
    val tag_values: String? = null, // jsonb
    val pdf_page_count: Int? = null,
    val modification_type: String? = null,
    val source_template_id: String? = null,
    val source_template_file_version: String? = null,
    val source_template_name: String? = null,
    val doc_id: String,
    val signing_request_id: String?
)
