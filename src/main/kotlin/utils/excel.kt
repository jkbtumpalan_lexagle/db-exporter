package utils

import org.apache.poi.ss.usermodel.CreationHelper
import org.apache.poi.xssf.usermodel.XSSFCellStyle
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.FileOutputStream
import java.util.Date
import kotlin.reflect.KProperty1

val mainWorkbook = XSSFWorkbook() //for XLSX
val signatoryWorksheet: XSSFSheet = mainWorkbook.createSheet("signatory")
val agreementWorksheet: XSSFSheet = mainWorkbook.createSheet("agreement")
const val excelFilepath = "./src/main/output/output.xlsx"
val createHelper: CreationHelper = mainWorkbook.creationHelper
val dateCellStyle: XSSFCellStyle = mainWorkbook.createCellStyle()

fun <T> setCellValue(data: T, excelRow: XSSFRow, col: Int) {
    val excelCol = excelRow.createCell(col)

    when(data) {
        is String -> excelCol.setCellValue(data)
        is Int -> excelCol.setCellValue(data.toDouble())
        is Double -> excelCol.setCellValue(data)
        is Boolean -> excelCol.setCellValue(data)
        is Date -> {
            excelCol.setCellValue(data)
            excelCol.cellStyle = dateCellStyle
        }
        else -> excelCol.setCellValue(data.toString())
    }
}

fun <T> processExcelContent(dataLabel: String, data: List<T>, memberProperties: Collection<KProperty1<T, *>>) {

    val worksheet = if (dataLabel == "signatory") {
        signatoryWorksheet
    } else {
        agreementWorksheet
    }

    var excelRow = worksheet.createRow(0)
    var propIndex = 0
    dateCellStyle.dataFormat = createHelper.createDataFormat().getFormat("m/d/yy")
    // Process Excel headers
    for(property in memberProperties) {
        setCellValue(property.name, excelRow, propIndex++)
    }

    // Process Excel Output
    data.forEachIndexed { index, it ->
        excelRow = worksheet.createRow(index + 1) //per row
        propIndex = 0

        for(property in memberProperties) {
            setCellValue(property.get(it), excelRow, propIndex++)
        }
    }
}

fun writeExcel(fileName: String, workbook: XSSFWorkbook){
    val outputStream = FileOutputStream(fileName)

    workbook.use {
        workbook.write(outputStream)
    }
}