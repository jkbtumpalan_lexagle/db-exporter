package utils

import com.opencsv.CSVWriter
import java.io.FileWriter
import kotlin.reflect.KProperty1

val getCSVFilePath = { dataName: String -> "./src/main/output/${dataName}-output.csv"}

fun <T> writeCSV(dataLabel: String, data: List<T>, memberProperties: Collection<KProperty1<T, *>>) {
    val fileWriter = FileWriter(getCSVFilePath(dataLabel))

    val csvWriter = CSVWriter(fileWriter,
        CSVWriter.DEFAULT_SEPARATOR,
        CSVWriter.NO_QUOTE_CHARACTER,
        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
        CSVWriter.DEFAULT_LINE_END
    )

    var headerData = arrayOf<String>()

    // Process CSV headers
    for(property in memberProperties) {
        headerData += property.name
    }
    csvWriter.use {
        csvWriter.writeNext(headerData)

        // Process CSV Body
        data.forEach{
            var instanceData = arrayOf<String>()
            for(property in memberProperties) {
                instanceData += property.get(it).toString()
            }
            csvWriter.writeNext(instanceData)
        }
    }
}