package utils

import data.Agreement
import data.Signatory
import java.sql.ResultSet
import java.time.OffsetDateTime

fun agreementMapper(result: ResultSet): MutableList<Agreement> {
    val agreementData = mutableListOf<Agreement>()

    while (result.next()) {
        val id = result.getString("id")
        val fileVersion = result.getInt("file_version")
        val fileName = result.getString("file_name")
        val downloadUrl = result.getString("download_url")
        val previewUrl = result.getString("preview_url")
        val version = result.getInt("version")
        val addedBy = result.getString("added_by")
        val addedOn = result.getObject("added_on", OffsetDateTime::class.java)
        val updatedBy = result.getString("updated_by")
        val updatedOn = result.getObject("updated_on", OffsetDateTime::class.java)
        val deletedBy: String? = result.getString("deleted_by")
        val deletedOn: OffsetDateTime? = result.getObject("deleted_on", OffsetDateTime::class.java)
        val isDeleted = result.getBoolean("is_deleted")
        val status = result.getString("status")
        val type: String? = result.getString("type")
        val workspaceId = result.getString("workspace_id")
        val roomId = result.getString("room_id")
        val roomName = result.getString("room_name")
        val activityId = result.getString("activity_id")
        val redline: String? = result.getString("redline") // jsonb
        val imageUrls: String? = result.getString("image_urls") // jsonb
        val signingMetadata: String? = result.getString("signing_metadata") // jsonb
        val sourceRoomId: String? = result.getString("source_room_id")
        val terminationReason: String? = result.getString("termination_reason")
        val sourceId: String? = result.getString("source_id")
        val sourceFileVersion: Int? = result.getInt("source_file_version")
        val fileSource = result.getString("file_source")
        val isCirculated = result.getBoolean("is_circulated")
        val tagValues: String? = result.getString("tag_values") // jsonb
        val pdfPageCount: Int? = result.getInt("pdf_page_count")
        val modificationType: String? = result.getString("modification_type")
        val sourceTemplateId: String? = result.getString("source_template_id")
        val sourceTemplateFileVersion: String? = result.getString("source_template_file_version")
        val sourceTemplateName: String? = result.getString("source_template_name")
        val docId = result.getString("doc_id")
        val signingRequestId: String? = result.getString("signing_request_id")

        agreementData.add(
            Agreement(
                id, fileVersion, fileName, downloadUrl, previewUrl, version, addedBy, addedOn!!,
                updatedBy, updatedOn!!, deletedBy, deletedOn, isDeleted, status, type, workspaceId,
                roomId, roomName, activityId, redline, imageUrls, signingMetadata, sourceRoomId,
                terminationReason, sourceId, sourceFileVersion, fileSource, isCirculated, tagValues,
                pdfPageCount, modificationType, sourceTemplateId, sourceTemplateFileVersion, sourceTemplateName,
                docId, signingRequestId
            )
        )
    }

    return agreementData
}

fun signatoryMapper(result: ResultSet): MutableList<Signatory> {
    val signatoryData = mutableListOf<Signatory>()

    while (result.next()) {
        val userId = result.getString("user_id")
        val version = result.getInt("version")
        val addedBy = result.getString("added_by")
        val addedOn = result.getObject("added_on", OffsetDateTime::class.java)
        val updatedBy = result.getString("updated_by")
        val updatedOn = result.getObject("updated_on", OffsetDateTime::class.java)
        val deletedBy: String? = result.getString("deleted_by")
        val deletedOn: OffsetDateTime? = result.getObject("deleted_on", OffsetDateTime::class.java)
        val isDeleted = result.getBoolean("is_deleted")
        val status = result.getString("status")
        val order = result.getInt("order")
        val id = result.getString("id")
        val type = result.getString("type")
        val signingRequestId = result.getString("signing_request_id")
        val info: String? = result.getString("info")

        signatoryData.add(
            Signatory(
                userId, version, addedBy, addedOn!!, updatedBy, updatedOn!!, deletedBy, deletedOn,
                isDeleted, status, order, id, type, signingRequestId, info
            )
        )
    }

    return signatoryData
}
